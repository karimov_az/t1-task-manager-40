package ru.t1.karimov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final int HALF_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES / 2;

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static List<Session> sessionList;

    @NotNull
    private static ISessionRepository sessionRepository;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession connection = connectionService.getSqlSession();

    @NotNull
    private static final IUserRepository userRepository = connection.getMapper(IUserRepository.class);

    @BeforeClass
    public static void createUsers() throws Exception {
        @NotNull final User user1 = new User();
        user1.setLogin("test1");
        user1.setPasswordHash("test1");
        userRepository.add(user1);
        connection.commit();
        USER1_ID = user1.getId();

        @NotNull final User user2 = new User();
        user2.setLogin("test2");
        user2.setPasswordHash("test2");
        userRepository.add(user2);
        connection.commit();
        USER2_ID = user2.getId();
    }

    @Before
    public void initRepository() throws Exception {
        sessionList = new ArrayList<>();
        sessionRepository = connection.getMapper(ISessionRepository.class);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            final boolean condition = i < NUMBER_OF_ENTRIES / 2;
            @NotNull final String userId = condition ? USER1_ID : USER2_ID;
            @NotNull final Session session = new Session(userId, Role.USUAL);
            sessionRepository.add(session);
            connection.commit();
            sessionList.add(session);
        }
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        userRepository.removeOneById(USER1_ID);
        connection.commit();
        userRepository.removeOneById(USER2_ID);
        connection.commit();
        connection.close();
    }

    @After
    public void initClear() throws Exception {
        for (@NotNull final Session session : sessionList) {
            @Nullable final String userId = session.getUserId();
            assertNotNull(userId);
            sessionRepository.removeOneById(userId, session.getId());
            connection.commit();
        }
        sessionList.clear();
    }

    @Test
    public void testAdd() throws Exception {
        final int expectedSize = sessionRepository.getSize();
        @NotNull final Session session = new Session(USER1_ID, Role.USUAL);
        sessionRepository.add(session);
        connection.commit();
        assertEquals(expectedSize + 1, sessionRepository.getSize());

        @Nullable final String userId = session.getUserId();
        assertNotNull(userId);
        sessionRepository.removeOneById(userId, session.getId());
        connection.commit();
    }

    @Test
    public void testClearForUserPositive() throws Exception {
        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            sessionRepository.removeAll(userId);
            connection.commit();
        }
        assertEquals(0, sessionRepository.getSizeByUserId(USER1_ID));
        assertEquals(0, sessionRepository.getSizeByUserId(USER2_ID));
    }

    @Test
    public void testClearForUserNegative() throws Exception {
        final int expectedSize = sessionRepository.getSize();
        @NotNull final String userId = UUID.randomUUID().toString();
        sessionRepository.removeAll(userId);
        connection.commit();
        assertEquals(expectedSize, sessionRepository.getSize());
    }

    @Test
    public void testFindByIdForUserPositive() throws Exception {
        for (@NotNull final Session session : sessionList) {
            @Nullable final String userId = session.getUserId();
            @NotNull final String id = session.getId();
            if (userId == null) continue;
            @Nullable final Session actualSession = sessionRepository.findOneById(userId, id);
            assertNotNull(actualSession);
            assertEquals(session.getId(), actualSession.getId());
        }
    }

    @Test
    public void testFindByIdForUserNegative() throws Exception {
        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final String userId = UUID.randomUUID().toString();
        assertNull(sessionRepository.findOneById(userId, id));
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        assertEquals(HALF_NUMBER_OF_ENTRIES, sessionRepository.getSizeByUserId(USER1_ID));
        assertEquals(HALF_NUMBER_OF_ENTRIES, sessionRepository.getSizeByUserId(USER2_ID));
    }

    @Test
    public void testRemoveByIdForUserPositive() throws Exception {
        for (@NotNull final Session session : sessionList) {
            @Nullable final String userId = session.getUserId();
            @NotNull final String id = session.getId();
            if (userId == null) continue;
            sessionRepository.removeOneById(userId, id);
            connection.commit();
            assertNull(sessionRepository.findOneById(userId, id));
        }
    }

    @Test
    public void testRemoveByIdForUserNegative() throws Exception {
        final int expectedSize = sessionRepository.getSize();
        @NotNull final String otherId = UUID.randomUUID().toString();
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        sessionRepository.removeOneById(otherUserId, otherId);
        assertEquals(expectedSize, sessionRepository.getSize());
    }

}
