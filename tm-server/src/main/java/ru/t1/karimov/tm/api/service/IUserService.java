package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    User add(@NotNull User model) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    @Nullable
    User findOneById(@Nullable String id) throws Exception;

    @Nullable
    User findOneByIndex(@Nullable Integer index) throws Exception;

    int getSize() throws Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    void lockUserByLogin(@Nullable String login) throws Exception;

    @Nullable
    User removeOne(@Nullable User model) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User removeOneByLogin(@Nullable String login) throws Exception;

    @Nullable
    User removeOneByEmail(@Nullable String email) throws Exception;

    void set(@NotNull Collection<User> users) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}
