package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.karimov.tm.api.endpoint.IUserEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.task.*;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.dto.request.user.UserRegistryRequest;
import ru.t1.karimov.tm.dto.request.user.UserRemoveRequest;
import ru.t1.karimov.tm.dto.response.task.*;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.marker.SoapCategory;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.service.PropertyService;

import java.util.List;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String adminToken;

    @Nullable
    private String testToken;

    @Nullable
    private Task task;

    @Before
    public void initTest() throws Exception {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        adminToken = adminResponse.getToken();

        @NotNull final UserRegistryRequest adminRegistryRequest = new UserRegistryRequest(adminToken);
        adminRegistryRequest.setLogin("tom");
        adminRegistryRequest.setPassword("tom");
        adminRegistryRequest.setEmail("tom@tst.ru");
        userEndpoint.registryUser(adminRegistryRequest);
        @NotNull final UserLoginResponse testResponse = authEndpoint.login(new UserLoginRequest("tom", "tom"));
        testToken = testResponse.getToken();

        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken);
        createRequest.setName("Test Task 1");
        createRequest.setDescription("Test Description 1");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        assertNotNull(createResponse);
        task = createResponse.getTask();
    }

    @After
    public void initEndTest() throws Exception {
        taskEndpoint.clearTask(new TaskClearRequest(testToken));

        @NotNull final UserLogoutRequest testRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(testRequest);

        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin("tom");
        assertNotNull(userEndpoint.removeUser(removeRequest));

        @NotNull final UserLogoutRequest adminRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(adminRequest);
    }

    @Test
    public void testChangeTaskStatusById() throws Exception {
        assertNotNull(task);
        @Nullable final String taskId = task.getId();
        @NotNull final Status newStatus = Status.IN_PROGRESS;

        @NotNull final TaskChangeStatusByIdRequest taskChangeStatusByIdRequest = new TaskChangeStatusByIdRequest(testToken);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setStatus(null);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setId(taskId);
        taskChangeStatusByIdRequest.setStatus(newStatus);
        @NotNull final TaskChangeStatusByIdResponse changeStatusByIdResponse = taskEndpoint.changeTaskStatusById(
                taskChangeStatusByIdRequest
        );
        @Nullable final Task actualTask = changeStatusByIdResponse.getTask();
        assertNotNull(actualTask);
        assertEquals(Status.IN_PROGRESS, actualTask.getStatus());
    }

    @Test
    public void testClearTask() throws Exception {
        @NotNull final TaskListRequest listRequest = new TaskListRequest(testToken, TaskSort.BY_CREATED);
        @Nullable List<Task> tasks = taskEndpoint.listTask(listRequest).getTaskList();
        assertNotNull(tasks);

        taskEndpoint.clearTask(new TaskClearRequest(testToken));
        tasks = taskEndpoint.listTask(listRequest).getTaskList();
        assertNull(tasks);
    }

    @Test
    public void testCreateTask() throws Exception {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(createRequest)
        );
        createRequest.setName("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(createRequest)
        );
        createRequest.setName("Test Task 2");
        createRequest.setDescription("Test Description 2");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        assertNotNull(createResponse);
        @Nullable Task actualTask = createResponse.getTask();
        assertNotNull(actualTask);
    }

    @Test
    public void testGetTaskById() throws Exception {
        assertNotNull(task);
        @NotNull final String id = task.getId();
        @NotNull final TaskGetByIdRequest taskGetByIdRequest = new TaskGetByIdRequest(testToken);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(taskGetByIdRequest)
        );
        taskGetByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.getTaskById(taskGetByIdRequest)
        );
        taskGetByIdRequest.setId("otherId");
        assertNull(taskEndpoint.getTaskById(taskGetByIdRequest).getTask());

        taskGetByIdRequest.setId(id);
        @NotNull final TaskGetByIdResponse taskGetByIdResponse = taskEndpoint.getTaskById(taskGetByIdRequest);
        assertNotNull(taskGetByIdResponse);
        @Nullable final Task actualTask = taskGetByIdResponse.getTask();
        assertNotNull(actualTask);
        assertEquals("Test Task 1", actualTask.getName());
    }

    @Test
    public void testGetTaskList() throws Exception {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(testToken);
        createRequest.setName("Task 2");
        createRequest.setDescription("Description 2");
        taskEndpoint.createTask(createRequest);
        createRequest.setName("Task 3");
        createRequest.setDescription("Description 3");
        taskEndpoint.createTask(createRequest);

        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(testToken, TaskSort.BY_NAME);
        @Nullable final List<Task> tasks = taskEndpoint.listTask(taskListRequest).getTaskList();
        assertNotNull(tasks);
        assertEquals(3, tasks.size());
    }

    @Test
    public void testRemoveTaskById() throws Exception {
        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(testToken, TaskSort.BY_NAME);
        @Nullable List<Task> tasks = taskEndpoint.listTask(taskListRequest).getTaskList();
        assertNotNull(tasks);
        assertEquals(1, tasks.size());
        @NotNull final String id = tasks.get(0).getId();

        @NotNull final TaskRemoveByIdRequest taskRemoveByIdRequest = new TaskRemoveByIdRequest(testToken);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest)
        );
        taskRemoveByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest)
        );
        taskRemoveByIdRequest.setId("otherId");
        assertNull(taskEndpoint.removeTaskById(taskRemoveByIdRequest).getTask());

        taskRemoveByIdRequest.setId(id);
        @NotNull final TaskRemoveByIdResponse taskRemoveByIdResponse =  taskEndpoint.removeTaskById(taskRemoveByIdRequest);
        assertNotNull(taskRemoveByIdResponse);
        tasks = taskEndpoint.listTask(taskListRequest).getTaskList();
        assertNull(tasks);
    }

    @Test
    public void testUpdateTaskById() throws Exception {
        assertNotNull(task);
        @Nullable String id = task.getId();

        @NotNull final TaskUpdateByIdRequest taskUpdateByIdRequest = new TaskUpdateByIdRequest(testToken);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(id);
        taskUpdateByIdRequest.setName("New task name");
        taskUpdateByIdRequest.setDescription("New description");
        @NotNull final TaskUpdateByIdResponse taskUpdateByIdResponse = taskEndpoint.updateTaskById(
                taskUpdateByIdRequest
        );
        assertNotNull(taskUpdateByIdResponse);
        @Nullable Task actualTask = taskUpdateByIdResponse.getTask();
        assertNotNull(actualTask);
        assertEquals(id, actualTask.getId());
        assertEquals("New task name", actualTask.getName());
        assertEquals("New description", actualTask.getDescription());

        taskUpdateByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId("otherId");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(id);
        taskUpdateByIdRequest.setName("");
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(null);
        assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
    }

}
